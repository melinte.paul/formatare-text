#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

/*
	Melinte Paul-Eduard, 312CD
*/

/*
	Genereaza urmatorul numar din string
	Folosita pentru parsarea comenzilor 
*/
int get_number(char **str)
{
	int number=0;

	if((*str)[0]=='\0')
	{
		return -2;
	}

	while(!isspace((*str)[0]) && (*str)[0]!='\0')
	{
		int x = (*str)[0]-'0';
		if((x>=0) && (x<=9))
		{
			number=number*10+x;
		}
		else 
		{
			while(!isspace((*str)[0]) && (*str)[0]!='\0')
				{
					(*str)++;
				}
			return -1;
		}
		(*str)++;
	}

	return number;
}

/*
	Verifica daca caracterul c 
	este tab sau space.
	Spre deosebire de functia isspace
	returneaza 0 pentru \n
*/
int is_space_tab(char c)
{
	if(c==' ' || c=='\t')
	{
		return 1;
	}

	return 0;
}


/*
	Functii de comparare folosite pentru ordonarea elementelor
	in generarea listelor ordonate
*/
int strcomp(const void *a, const void *b)
{
	return strcmp((char *)a,(char *)b);
}

int neg_strcomp(const void *a, const void *b)
{
	return -strcmp((char *)a,(char *)b);
}

/*
	Gaseste cea mai lunga linie. Folosita de functiile de
	align right, centering si justify
*/
int get_longest_line(char text[1000][1000],int line_count)
{
	int max=0,i;

	for(i=0;i<line_count;i++)
	{
		if(max<strlen(text[i]))
			max=strlen(text[i]);
	}

	return max;
}

/*
	Returneaza lungimea celei mai lungi secvente de 
	' ' sau '\t'
	Folosita de functia justify
*/
int get_longest_ws(char str[1000])
{
	int max=0;
	int aux=0;

	while(str[0]!='\0')
	{
		if(is_space_tab(str[0]))
		{
			aux++;
		}
		else 
		{
			if(aux>max)
			{
				max=aux;
			}
			aux=0;
		}

		str++;
	}

	return max;
}

/*
	Distruge leading whitespaceul
	Folosita pentru alinierea la stanga
*/
void remove_leading_ws(char str[1000])
{
	int i=0;
	while(is_space_tab(str[i]))
	{
		i++;
	}
	memmove(str,str+i,strlen(str)-i+1);
}

/*
	Adauga ' ' la inceputul stringului
	Folosita pentru alinierea la dreapta,
	centering si justify
*/
void add_leading_ws(char str[1000], int count)
{
	memmove(str+count,str,strlen(str)+1);
	memset((void *)str,' ',count);
}

/*
	Distruge trailing whitespace in string
*/
void remove_trailing_space(char str[1000])
{
	int i=strlen(str)-1,j;
	while(str[i]=='\n'){
		i--;
	}
	j=i+1;
	while(isspace(str[i]))
	{
		i--;
	}
	strcpy(str+i+1,str+j);
}

/*
	Distruge trailing whitespace-ul din tot textul
*/
void remove_trailing_spaces(char text[1000][1000], int line_count)
{
	int i;

	for(i=0;i<line_count;i++)
	{
		remove_trailing_space(text[i]);
	}
}

/*
	Verifica daca stringul este sfarsitul unui paragraf
*/
int is_eo_paragraph (char str[1000])
{
	if(str[0]=='\n')
	{
		return 1;
	}

	return 0;
}

/*
	Imparte a la b si adauga 1 daca impartirea nu este exacta
	Folosita pentru centering
*/
int up_division(int a, int b)
{
	int aux=a/b;

	if(aux*b==a)
	{
		return aux;
	}

	else return aux+1;
}

/*
	Functia wrap. Returneaza numarul nou de linii ale textului
*/
int wrap(char text[1000][1000], int line_count, int new_length)
{
	char new_text[1000][1000];
	char buf[2000];
	int i,new_line_count=0,j;
	int pos;

	/*
		Fiecare linie este adaugata intr-un string buf, din care 
		sunt extrase liniile noului text
		Prima linie este procesata separat
	*/

	strcpy(buf,text[0]);
	while(strlen(buf)>new_length)
	{
		pos=new_length;
		while(!isspace(buf[pos])){
			pos--;
		}
		pos++;
		strncpy(new_text[new_line_count++],buf,pos);
		memmove(buf,buf+pos,strlen(buf+pos)+1);
		new_text[new_line_count-1][pos-1]='\n';
		new_text[new_line_count-1][pos]='\0';
	}

	for(i=1;i<line_count;i++)
	{
		/*
			In cazul in care am ajuns la capatul paragrafului,
			copiem elementele ramase din buf intr-o noua linie a 
			textului
		*/
		if(is_eo_paragraph(text[i]))
		{
			strcpy(new_text[new_line_count++],buf);
			new_text[new_line_count-1][strlen(buf)-1]='\n';
			new_text[new_line_count-1][strlen(buf)]='\0';
			strcpy(new_text[new_line_count++],text[i]);
			for(j=0;j<1000;j++)
			{
				buf[j]='\0';
			}
		}
		else
		{
			/*
				Adaugam o linie din textul vechi la buf
			*/
			remove_trailing_space(buf);
			buf[strlen(buf)-1]=' ';
			buf[strlen(buf)]='\0';

			if(buf[0]!='\0')
			{
				remove_leading_ws(text[i]);
			}
			strcat(buf,text[i]);
			/*
				Cat timp buf este destul de lung pentru a extrage noi
				linii, adaugam linii noi in textul nou
			*/
			while(strlen(buf)>new_length)
			{
				pos=new_length;
				while(!isspace(buf[pos]))
				{
					pos--;
				}
				pos++;

				strncpy(new_text[new_line_count++],buf,pos);
				memmove(buf,buf+pos,strlen(buf+pos)+1);
				new_text[new_line_count-1][pos-1]='\n';
				new_text[new_line_count-1][pos]='\0';
			}
		}
	}
	/*
		Adaugam capatul ultimului paragraf la textul nou
	*/
	strcpy(new_text[new_line_count++],buf);
	new_text[new_line_count-1][strlen(buf)-1]='\n';
	new_text[new_line_count-1][strlen(buf)]='\0';

	/*
		Copiem textul nou in zona de memorie a textului vechi
	*/
	for(i=0;i<new_line_count;i++)
	{
		strcpy(text[i],new_text[i]);
	}

	return new_line_count;
}
/*
	Functie ajutatoare pentru wrap. Calculeaza noul wrap_length
	si apoi foloseste punctia wrap pentru a aduce textul la 
	forma ceruta
*/
void handle_wrap(char *comm, char text[1000][1000], int *line_count)
{
	comm++;
	while(comm[0]==' ')
	{
		comm++;
	}

	int new_length=get_number(&comm);

	int new_line_count=wrap(text,*line_count,new_length);
	*line_count=new_line_count;
}

/*
	Alinierea la stanga se face prin stergerea leading whitespace-ului
*/
void left_align(char text[1000][1000], int first_line, int last_line)
{
	int i;

	for(i=first_line;i<=last_line;i++)
	{
		remove_leading_ws(text[i]);
	}
}

/*
	Functie ajutatoare pentru left_align. Genereaza limitele in care 
	se face alinierea
*/
void handle_left(char *comm, char text[1000][1000], int line_count)
{
	int first_line;
	comm++;
	while(comm[0]==' ')
	{
		comm++;
	}

	first_line=get_number(&comm);

	int last_line;

	while(comm[0]==' ')
	{
		comm++;
	}

	last_line=get_number(&comm);

	if(first_line==-1 || last_line==-1)
	{
		printf("Invalid operation!\n");
		return;
	}

	if(first_line==-2)
	{
		first_line=0;
	}

	if(last_line==-2)
	{
		last_line=line_count-1;
	}

	left_align(text,first_line,last_line);

}

/*
	Alinierea la dreapta se face prin adaugarea de whitespace
	pana liniile au aceasi lungime
*/
void right_align(char text[1000][1000],int first_line
				,int last_line,int line_count)
{
	int i;
	int max=get_longest_line(text,line_count);
	for(i=first_line;i<=last_line;i++)
	{
		add_leading_ws(text[i],max-strlen(text[i]));
	}
}

/*
	Functie ajutatoare pentru right_align ce genereaza
	randurile intre care se face alinierea
*/
void handle_right(char *comm, char text[1000][1000], int line_count)
{
	int first_line;
	comm++;
	while(comm[0]==' ')
	{
		comm++;
	}

	first_line=get_number(&comm);

	int last_line;

	while(comm[0]==' ')
	{
		comm++;
	}

	last_line=get_number(&comm);

	if(first_line==-1 || last_line==-1)
	{
		printf("Invalid operation!\n");
		return;
	}

	if(first_line==-2)
	{
		first_line=0;
	}

	if(last_line==-2)
	{
		last_line=line_count-1;
	}
	remove_trailing_spaces(text,line_count);
	right_align(text,first_line,last_line,line_count);
}

/*
	Centrarea textului se face prin adaugarea de whitespace
*/
void center_align(char text[1000][1000],int first_line
				,int last_line,int line_count)
{
	int i;
	int max=get_longest_line(text,line_count);
	for(i=first_line;i<=last_line;i++)
	{
		add_leading_ws(text[i],up_division(max-strlen(text[i]),2));
	}
}

/*
	Functie ajutatoare ce genereaza capetele alinierii
*/
void handle_center(char *comm, char text[1000][1000], int line_count)
{
	int first_line;
	comm++;
	while(comm[0]==' ')
	{
		comm++;
	}

	first_line=get_number(&comm);

	int last_line;

	while(comm[0]==' ')
	{
		comm++;
	}

	last_line=get_number(&comm);

	if(first_line==-1 || last_line==-1)
	{
		printf("Invalid operation!\n");
		return;
	}

	if(first_line==-2)
	{
		first_line=0;
	}

	if(last_line==-2)
	{
		last_line=line_count-1;
	}

	remove_trailing_spaces(text,line_count);
	center_align(text,first_line,last_line,line_count);

}

void justify(char text[1000][1000],int first_line
			, int last_line,int line_count)
{
	int i;
	int max=get_longest_line(text,line_count);

	left_align(text,first_line,last_line);

	for(i=first_line;i<last_line;i++)
	{
		/*
			Daca linia curenta sau cea urmatoare e final de paragraf,
			lasam alinierea la stanga
		*/
		if(is_eo_paragraph(text[i]) || is_eo_paragraph(text[i+1]))
		{
			continue;
		}

		/*
			In dif gasim cate spatii trebuie adaugate in total
			In longest este salvata lungimea celui mai lung whitespace,
			lungime la care vor fi aduse cat mai multe whitespaceuri initial
		*/
		int dif=max-strlen(text[i]);
		int longest=get_longest_ws(text[i]);

		char *str=text[i];
		int count=0;
		while(str[0]!='\0' && dif)
		{
			if(is_space_tab(str[0]))
			{
				count++;
			}
			else
			{
				if(count<longest && count)
				{
					add_leading_ws(str,1);
					str++;
					dif--;
				}
				count=0;
			}
			str++;
		}

		/*
			Daca, dupa aducerea la aceiasi lungime a spatiilor,
			mai raman spatii de adaugat, parcurgem stringul si adaugam
			spatii in fata cuvintelor pana am ajuns la lungimea dorita
		*/
		while(dif)
		{
			str=text[i]+1;
			while(str[0]!='\n' && dif)
			{
				if(is_space_tab((str-1)[0]) && !is_space_tab(str[0]))
				{
					add_leading_ws(str,1);
					str++;
					dif--;
				}
				str++;
			}
		}
	}

}

/*
	Functie ajutatoare ce genereaza capetele alinierii
*/
void handle_justify(char *comm, char text[1000][1000], int line_count)
{
	int first_line;
	comm++;
	while(comm[0]==' ')
	{
		comm++;
	}

	first_line=get_number(&comm);

	int last_line;

	while(comm[0]==' ')
	{
		comm++;
	}

	last_line=get_number(&comm);

	if(first_line==-1 || last_line==-1)
	{
		printf("Invalid operation!\n");
		return;
	}

	if(first_line==-2)
	{
		first_line=0;
	}

	if(last_line==-2)
	{
		last_line=line_count-1;
	}

	remove_trailing_spaces(text,line_count);
	justify(text,first_line,last_line,line_count);

}

/*
	Indentarea paragrafelor se face prin adaugarea de
	whitespace in cantitatea dorita pe liniile de dupa finaluri
	de paragraf
*/
void indent_paragraph(char text[1000][1000], int ind_count
					, int first_line, int last_line)
{
	int i;

	if(first_line==0)
	{
		add_leading_ws(text[0],ind_count);
		first_line++;
	}

	for(i=first_line;i<=last_line;i++)
	{
		if(is_eo_paragraph(text[i-1]))
		{
			add_leading_ws(text[i],ind_count);
		}
	}
}

/*
	Functie ajutatoare ce genereaza capetele operatiei
*/
void handle_paragraph(char *comm, char text[1000][1000], int line_count)
{
	int ind_count;
	comm++;
	while(comm[0]==' ')
	{
		comm++;
	}

	ind_count=get_number(&comm);

	int first_line;
	while(comm[0]==' ')
	{
		comm++;
	}

	first_line=get_number(&comm);

	int last_line;

	while(comm[0]==' ')
	{
		comm++;
	}

	last_line=get_number(&comm);

	if(first_line==-1 || last_line==-1)
	{
		printf("Invalid operation!\n");
		return;
	}

	if(first_line==-2)
	{
		first_line=0;
	}

	if(last_line==-2)
	{
		last_line=line_count-1;
	}

	remove_trailing_spaces(text,line_count);
	indent_paragraph(text,ind_count,first_line,last_line);

}

/*
	Genereaza un element pentru listele numerotate
*/
void get_numbered_elem(char str[1000],int n, char special)
{
	int i=0;

	while(n!=0)
	{
		memmove(str+1,str,strlen(str)+1);
		str[0]='0'+(char)n%10;
		n=n/10;
		i++;
	}
	str[i]=special;
	str[i+1]=' ';
	str[i+2]='\0';
}

void add_list(char text[1000][1000],int first_line, int last_line
				, char type, char special)
{
	int i;
	char chr;
	left_align(text,first_line,last_line);
	switch(type)
	{
	case 'b':
		for(i=first_line;i<=last_line;i++)
		{
			char tmp[1000];
			tmp[0]=special;
			tmp[1]=' ';
			tmp[2]='\0';
			strcat(tmp,text[i]);
			strcpy(text[i],tmp);
		}
		break;
	case 'a':
	case 'A':
		chr=type;
		for(i=first_line;i<=last_line;i++)
		{
			char tmp[1000];
			tmp[0]=chr;
			tmp[1]=special;
			tmp[2]=' ';
			tmp[3]='\0';
			strcat(tmp,text[i]);
			strcpy(text[i],tmp);
			chr++;
		}
		break;
	case 'n':
		for(i=first_line;i<=last_line;i++)
		{
			char tmp[1000];
			get_numbered_elem(tmp,i-first_line+1,special);
			strcat(tmp,text[i]);
			strcpy(text[i],tmp);
		}
		break;
	}
}

/*
	Functie ajutatoare ce calculeaza capetele, tipul si caracterul special
	pentru generarea listelor neordonate
*/
void handle_list(char *comm, char text[1000][1000], int line_count)
{
	char type,special;

	comm++;
	while(comm[0]==' ')
	{
		comm++;
	}
	type=comm[0];

	comm++;
	while(comm[0]==' ')
	{
		comm++;
	}
	special=comm[0];
	
	comm++;
	int first_line;
	while(comm[0]==' ')
	{
		comm++;
	}
	first_line=get_number(&comm);

	int last_line;
	while(comm[0]==' ')
	{
		comm++;
	}
	last_line=get_number(&comm);

	if(first_line==-1 || last_line==-1)
	{
		printf("Invalid operation!\n");
		return;
	}

	if(first_line==-2)
	{
		first_line=0;
	}

	if(last_line==-2)
	{
		last_line=line_count-1;
	}

	add_list(text,first_line,last_line,type,special);
}

/*
	Functia calculeaza tipul, caracterul special, ordinea si capetele 
	listelor ordonate
*/

void handle_ordered(char *comm, char text[1000][1000], int line_count)
{
	char type,special,order;

	comm++;
	while(comm[0]==' ')
	{
		comm++;
	}
	type=comm[0];

	comm++;
	while(comm[0]==' ')
	{
		comm++;
	}
	special=comm[0];

	comm++;
	while(comm[0]==' ')
	{
		comm++;
	}
	order=comm[0];

	
	comm++;
	int first_line;
	while(comm[0]==' ')
	{
		comm++;
	}
	first_line=get_number(&comm);

	int last_line;
	while(comm[0]==' ')
	{
		comm++;
	}
	last_line=get_number(&comm);

	if(first_line==-1 || last_line==-1)
	{
		printf("Invalid operation!\n");
		return;
	}

	if(first_line==-2)
	{
		first_line=0;
	}

	if(last_line==-2)
	{
		last_line=line_count-1;
	}

	/*
		Zona care va fi transformata intr-o lista ordonata
		este sortata fie ascendent, fie descendent
	*/
	switch(order)
	{
		case 'a':
			qsort(text+first_line,last_line-first_line+1,1000,strcomp);
			break;
		case 'z':
			qsort(text+first_line,last_line-first_line+1,1000,neg_strcomp);
	}

	/*
		Dupa sortare, folosim aceiasi functie ca si la listele neordonate
	*/
	add_list(text,first_line,last_line,type,special);
}

void handle_comm(char *comm, char text[1000][1000], int *line_count)
{
	while(comm[0]==' ')	
	{
		comm++;
	}
	/*
	Verificam corectitudinea comenzii
	*/
	if(strlen(comm)>20 || (!isspace(comm[1]) && comm[1]!='\0'))
	{
		printf("Invalid operation!\n");
		return;
	}
	/*
	In functie de comanda data folosim un numar de functii ajutatoare
	*/
	switch(comm[0])
	{
		case 'W':
		case 'w':
			handle_wrap(comm, text, line_count);
			break;
		case 'C':
		case 'c':
			handle_center(comm, text, *line_count);
			break;
		case 'L':
		case 'l':			
			handle_left(comm, text, *line_count);
			break;
		case 'R':
		case 'r':
			handle_right(comm, text, *line_count);
			break;
		case 'J':
		case 'j':
			handle_justify(comm, text, *line_count);
			break;
		case 'P':
		case 'p':
			handle_paragraph(comm, text, *line_count);
			break;
		case 'I':
		case 'i':
			handle_list(comm, text, *line_count);
			break;
		case 'O':
		case 'o':
			handle_ordered(comm, text, *line_count);
			break;
		default:
			printf("Invalid operation!\n");
	}
	
	remove_trailing_spaces(text,*line_count);

}


int main(int argc, char *argv[]) {
	char buf[1000],
	text[1000][1000];
	int line_count = 0,
	i;
	/*
	Citirea textului si afisarea sa sunt facut folosing scheletul oferit in
	enunt. Singura modificare facuta este faptul ca citirea si afisarea se fac
	in si din acelasi vector de stringuri
	*/
	if (argc != 4) 
	{
		fprintf(stderr,
		"Usage: %stderr operations_string input_file output_file\n",
		argv[0]);
		return -1;
	}

	FILE *input_file = fopen(argv[2], "r");

	if (input_file == NULL) 
	{
		fprintf(stderr, "File \"%s\" not found\n", argv[2]);
		return -2;
	}

	while (fgets(buf, 1000, input_file)) 
	{
		strcpy(text[line_count], buf);
		line_count++;
	}

	fclose(input_file);

	char commands[1000];
	strcpy(commands,argv[1]);
	/*
	Separam in stringul comm fiecare comanda data in cli
	*/
	char *comm = strtok(commands,",\0");
	while(comm)
	{
		/*
		Fiecare comanda este prelucrata separat de functia handle_comm
		*/
		handle_comm(comm,text,&line_count);
		comm = strtok(NULL,",\0");
	}

	FILE *output_file = fopen(argv[3], "w");

	if (output_file == NULL)
	{
		fprintf(stderr, "Could not open or create file \"%s\"\n", argv[3]);
		return -2;
	}

	for (i = 0; i < line_count; i++) 
	{
		fputs(text[i], output_file);
	}

	fclose(output_file);

	return 0;
}
